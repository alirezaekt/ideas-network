import { v4 as uuid } from "uuid"
const useToasts = () => {
    const toasts = useState("toasts",() => []);
    
    class ToastMessage {
        content: any
        duration: number
        id: string
        timer: any
        constructor(content,duration) {
            this.content = content
            this.duration = duration
            this.id = uuid()
            this.timer = setTimeout(()=>{
                this.remove()
            },this.duration)
        }
        remove() {
            removeToast(this)
        }
    }

    const makeToast = (content, duration = 3000) => {
        let to = new ToastMessage(content, duration)
        toasts.value.push(to)
    }

    const removeToast = (toast) => {
        let index = toasts.value.map(e => e.id).indexOf(toast.id);
        toasts.value.splice(index,1)
    }

    return {
        toasts,
        makeToast,
        removeToast
    };
};

export default useToasts;
