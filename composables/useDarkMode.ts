const useDarkMode = () => {
    const isDarkMode = useState("darkMode",() => false);
    const toggleDarkMode = () => {
        isDarkMode.value = !isDarkMode.value

        //storing in cookie
        const cookie = useCookie("theme")
        if(isDarkMode.value === false) {
            cookie.value = "light"
        } else {
            cookie.value = "dark"
        }
            
    }
    const setDarkMode = (isDark:string) => {
        if (isDark === "dark") {
            isDarkMode.value = true
        } else {
            isDarkMode.value = false
        }
    }
    return {
        isDarkMode,
        toggleDarkMode,
        setDarkMode
    };
};

export default useDarkMode;
