const useAuth = () => {
    
    const supabase = useSupabaseClient()
    
    const signUp = async ({ email, password, ...metadata }) => {
        const { user:u, error } = await supabase.auth.signUp({ email, password },{ data: metadata })
        if (error) throw error
        
        await supabase.from("profiles").insert({
            user_id:u.id,
            username:u.user_metadata.username
        })
        if (error) throw error
        return u
    }

    const signIn = async ({email, password}) => {
        const { user:u, error } = await supabase.auth.signIn({ email, password })
        if(error) throw error
        return u
    }

    const forgotPassword = async (email) => {
        const { data, error } = await supabase.auth.api.resetPasswordForEmail(email)
        return {data, error}
    }
    const updatePassword = async (access_token, new_password) => {
        const { data, error } = await supabase.auth.api.updateUser(access_token, { password : new_password })
        return {data, error}
    }

    const oSignIn =async (provider:'github'|'google'|'gitlab'|'bitbucket') => {
        const { user:u, error } = await supabase.auth.signIn({ provider: provider })
        if(error) throw error
        return u
    }

    const signOut = async () => {
        const { error } = await supabase.auth.signOut()
        if ( error ) throw error
    }

    const updateUsername = async (new_username) => {
        //not tested
        const { user:u, error } = await supabase.auth.update({data:{username:new_username}})
        await supabase.from("profiles").update({
            username:u.user_metadata.username
        }).match({'user_id' : u.id})
    }

    return {
        signUp,
        signIn,
        signOut,
        forgotPassword,
        updatePassword,
        oSignIn,
        updateUsername
    }
}

export default useAuth
