const useSocial = () => {
    
    const supabase = useSupabaseClient()
    const user = useSupabaseUser()
    const router = useRouter()

    const getFeed = async () => {
        let {data,error} = await supabase
        .from("ideas")
        .select('*')
        .order('created_at',{ascending:false})
        .range(0,10)
        if (error) throw error
        data = await Promise.all(data.map(async (post)=>{
            const [{data:username},{data: reactions}] = await Promise.all([
                await supabase
                .from('profiles')
                .select('username').eq('user_id',post.user_id).maybeSingle(),
                await supabase
                .from('reactions')
                .select('user_id, content').eq('post',post.id).limit(100)
            ])
            return{
                ...post, username:username['username'], reactions
            }
        }))
        return data
    }
    const getTrendingFeed = async () => {
        let {data,error} = await supabase
        .from("ideas")
        .select('*')
        .order('created_at',{ascending:false})
        .range(0,10)
        if (error) throw error
        data = await Promise.all(data.map(async (post)=>{
            const [{data:username},{data: reactions}] = await Promise.all([
                await supabase
                .from('profiles')
                .select('username').eq('user_id',post.user_id).maybeSingle(),
                await supabase
                .from('reactions')
                .select('user_id, content').eq('post',post.id).limit(100)
            ])
            return{
                ...post, username:username['username'], reactions
            }
        }))
        return data
    }

    const getPostsByUserID = async (user_id) => {
        let { data, error } = await supabase
        .from("ideas")
        .select()
        .order('created_at',{ascending:false})
        .match({'user_id' : user_id})
        .limit(10)
        if (error) throw error
        data = await Promise.all(data.map(async (post)=>{
            const [{data: reactions}] = await Promise.all([
                await supabase
                .from('reactions')
                .select('user_id, content').eq('post',post.id).limit(100)
            ])
            return{
                ...post, reactions
            }
        }))
        return { data,error }
    }

    //reactions
    const alreadyHasReaction = async (post) => {
        if (user.value) {
            let { data, error } = await supabase
            .from("reactions")
            .select('content')
            .match({'post' : post})
            .match({'user_id' : user.value.id})
            if (error) throw error
            let hasReaction = data[0]
            return { hasReaction, error }
        } else {
            return { error:'Not logged in' }
        }
    }
    
    const createReaction = async (post,content) => {
        if (user){
            const { data, error } = await supabase
            .from('reactions')
            .insert({post, user_id: user.value.id, content})
        } else {
            router.push('/login')
        }
    }

    const removeReaction = async (post,content) => {
        if (user){
            const { data, error } = await supabase
            .from('reactions')
            .delete()
            .match({post: post, user_id: user.value.id})
            return { data, error }
        } else {
            router.push('/login')
        }
    }

    const removeIdea = async (idea_id) => {
        const { data, error } = await supabase
        .from("ideas")
        .delete()
        .match({ id : idea_id})
        return {data, error}
    }

    const usernameAvailable = async (username) => {
        const { data, error } = await supabase
        .from('profiles')
        .select('username')
        .eq('username', username)
        if (username.length < 4){
            return false
        }
        if (data.length === 1) {
            return false
        } else {
            return true
        }
    }

    const reportPost = async (reportedpost, reason) => {
        if (user) {
            const { data, error } = await supabase
            .from('reports')
            .insert({user: user.value.id, reported_post: reportedpost, reason: reason})
            return { data, error }
        } else {
            router.push('/login')
        }
    }

    const deleteAccount = async () => {
        if (user){
            // delete user reactions
            // const { error } = await supabase
            // .from('reactions')
            // .delete()
            // .match({ user: user.value.id })
            // console.log({ error }) 
            // delete reactions on user posts
            // let { data:userposts, error:error_2 } = await supabase
            // .from("ideas")
            // .select('id')
            // .match({'user_id' : user.value.id})
            
            
            // const { error:error_3 } = await supabase
            // .from('reactions')
            // .delete()
            // .match({ post: user.value.id })
            // console.log({ error }) 
            //delete all posts
            // const { error:error_4 } = await supabase
            // .from('ideas')
            // .delete()
            // .match({ user_id: user.value.id })
            // console.log({ error_4 }) 
            //delete profile
            // const { error: error_2 } = await supabase
            // .from('profiles')
            // .delete()
            // .match({ user_id: user.value.id })
            // console.log({ error_2 }) 
            //delete user
            // const { data: user_g, error:error_3 } = await supabase.auth.api
            // .deleteUser(
            //     user.value.id
            // )
            // console.log({ error_3 }) 
            // router.push('/')
        } else {
            router.push('/login')
        }
    }

    return {
        getFeed,
        getTrendingFeed,
        alreadyHasReaction,
        createReaction,
        removeReaction,
        usernameAvailable,
        getPostsByUserID,
        removeIdea,
        reportPost,
        deleteAccount
    }
}

export default useSocial
