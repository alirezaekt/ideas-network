# Ideas Network

A calm inspiring social platform - find and share ideas. Made with NuxtJS and Supabase.

Check out the [Demo](https://ideas.coolconcepts.tech/)

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```
set the API keys in .env file:
```
SUPABASE_URL=***
SUPABASE_KEY=***

```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

