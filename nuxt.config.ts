import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  meta:{
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.sandbox.google.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' },
    ]
  },
  css: [
    'assets/css/global.scss'
  ],
  modules: [
    '@nuxtjs/supabase'
  ],
  buildModules: [
    '@nuxtjs/tailwindcss'
  ],
  runtimeConfig: {
    public: {
      frontendUrl: process.env.FRONTEND_URL
    }
  }

})
